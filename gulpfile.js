'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var clean = require('gulp-clean');
var del = require('del');
const imagemin = require('gulp-imagemin');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');

const dir = {
  src: './',
  build: 'dist'
};

sass.compiler = require('node-sass');


function style() {
  return gulp.src('./scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
}
exports.style = style;

function watch() {
  browserSync.init({
    server: {
      baseDir: "./",
      index: "index.html"
    }
  });
  gulp.watch('./scss/**/*.scss', style);
  gulp.watch('./*.html').on('change', browserSync.reload);
  gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}
exports.watch = watch;


function cleaning() {
  return del([dir.build]);
}
exports.cleaning = cleaning;



function workImages() {
  return gulp.src('./img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));

};
exports.workImages = workImages;


function workHtmlJs() {
  return gulp.src('./*.html')
    .pipe(usemin({
      css: [rev()],
      html: [htmlmin({
        collapseWhitespace: true
      })],
      js: [uglify(), rev()],
      inlinejs: [uglify()],
      inlinecss: [cleanCss(), 'concat']
    }))
    .pipe(gulp.dest(dir.build))
};
exports.workHtmlJs = workHtmlJs;


function build(done) {
    gulp.series(style, cleaning, gulp.parallel(workImages, workHtmlJs))();
    done()
};
exports.build = build;
exports.default = build;
