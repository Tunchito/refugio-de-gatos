$(function() {
  var selectedClass = "";
  $(".filter").click(function() {
    selectedClass = $(this).attr("data-rel");
    $("#gallery").fadeTo(100, 0.1);
    $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
    setTimeout(function() {
      $("." + selectedClass).fadeIn().addClass('animation');
      $("#gallery").fadeTo(300, 1);
    }, 300);
  });
});

// Tooltips Initialization and Carousel
$(function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('.carousel').carousel({
    interval: 2000
  });
  $('#modalLoginForm').on('show.bs.modal', function(e) {
    console.log('el modal se muestra');
    $('#btnContacto').removeClass('btn-info');
    $('#btnContacto').addClass('btn-secondary');
    $('#btnContacto').prop('disabled', true);
  });
  $('#modalLoginForm').on('hidden.bs.modal', function(e) {
    console.log('el modal se oculta');
    $('#btnContacto').removeClass('btn-secondary');
    $('#btnContacto').addClass('btn-info');
    $('#btnContacto').prop('disabled', false);
  });
});
